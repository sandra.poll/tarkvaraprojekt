<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//get logged in user info
Route::middleware('auth:api')->get('/users/me', function (Request $request) {
    return $request->user();
});
//authentication
Route::middleware('api')->post('/auth', 'Auth\LoginController@authenticate');
//candidate store
Route::middleware('auth:api')->post('/candidates', 'CandidatedController@store');
//candidate delete
Route::middleware('auth:api')->delete('/candidates/{id}', 'CandidatedController@destroy');
//get candidates
Route::middleware('auth:api')->get('/users/candidates', 'CandidatedController@getCandidates');
//accept candidate
Route::middleware('auth:api')->post('/candidates/{user}/accept', 'CandidatedController@acceptCandidate');
//get contact data
Route::middleware('auth:api')->get('/contactInfo', 'ContactController@getContactData');
//change contact info
Route::middleware('auth:api')->post('/contactInfo/', 'ContactController@changeContactData');
//get votes
Route::middleware('auth:api')->get('/votes', 'VotesController@all');
Route::middleware('auth:api')->get('/last-votes', 'VotesController@getLastVotes');
//update vote
Route::middleware('auth:api')->put('/votes/{vote}', 'VotesController@update');
//positive votes
Route::middleware('auth:api')->get('/votes/{vote}/yes', 'VotesController@addPositiveVote');
//negative vote
Route::middleware('auth:api')->get('/votes/{vote}/no', 'VotesController@addNegativeVote');
//Delete vote
Route::middleware('auth:api')->delete('/votes/{vote}', 'VotesController@destroy');
//votes store
Route::middleware('auth:api')->post('/votes', 'VotesController@store');
//votes delete
Route::middleware('auth:api')->delete('/votes', 'VotesController@destroy');
//info members
Route::middleware('api')->get('/users/members', 'InfoController@getMembers');
//get info text
Route::middleware('api')->get('/infoPage', 'InfoController@getInfoText');
//change info text
Route::middleware('api')->post('/infoPage/', 'InfoController@changeInfoText');
//candidate delete
Route::middleware('auth:api')->delete('/members/{id}', 'InfoController@destroy');

Route::middleware('api')->get('/events', 'CalendarController@events');

//get info to sidebar
Route::middleware('api')->get('/sidebarInfo', 'InfoController@getInfo');
