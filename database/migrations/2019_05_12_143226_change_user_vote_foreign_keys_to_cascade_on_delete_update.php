<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserVoteForeignKeysToCascadeOnDeleteUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_vote', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['vote_id']);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('vote_id')->references('id')->on('votes')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_vote', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['vote_id']);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('vote_id')->references('id')->on('votes')->onDelete('restrict')->onUpdate('restrict');
        });
    }
}
