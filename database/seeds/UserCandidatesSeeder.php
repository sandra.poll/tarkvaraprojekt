<?php

use App\Candidate;
use App\User;
use Illuminate\Database\Seeder;

class UserCandidatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 500; $i++) {
            $user = new User();
            $user->name = $faker->name;
            $user->email = $faker->email;
            $user->job_title = 'Õpilane';
            $user->role = 'user';

            $user->save();

            $candidation = new Candidate();
            $candidation->user()->associate($user);
            $candidation->save();
        }
    }

}
