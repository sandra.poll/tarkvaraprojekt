<?php

use App\Vote;
use Illuminate\Database\Seeder;

class VotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 25; $i++) {
            $vote = new Vote();
            $vote->title = $faker->words(3, true);
            $vote->description = $faker->text;
            $vote->created_by = 1;

            $vote->save();
        }
    }

}
