<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('options')->insert([
            ['value' => ' ', 'kkk' => 'infoDescription'],
            ['value' => ' ', 'kkk' => 'infoProjects'],
            ['value' => '372 5000 0000', 'kkk' => 'ContactNumber'],
            ['value' => 'info@ametikool.ee', 'kkk' => 'ContactEmail'],
            ['value' => 'Vallimaa 5a, Kuressaare, Saare maakond, Eesti', 'kkk' => 'ContactAddress'],
        ]);
    }
}
