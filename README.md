Projekt asub lehel: https://tarkvaraprojekt.vilbas.ee/

Projekt on loodud Kuresaare Ametikooli õpilasesinduse tarbeks. 
Lehe tavakasutajad on õpilased, moderaatori õigustega kasutajad on õpilasesinduse liikmed ning admin õigus on õpilasesinduse esindajal ning lehe loojatel.

Lehel on võimalik luua hääletusi, hääletada, õpilasesindusse kandideerida, õpilasi õpilasesindusse võtta/eemaldada ning informatsiooni vaadata/muuta.

Sisselogimine tavaliselt käib Microsoft-i oAuth kaudu. Demonstreerimise eesmärgil oleme loonud 3 demo kasutajat.

Demo kasutajad:
*  admin@demo.com
*  moderator@demo.com
*  user@demo.com