@servers(['web' => 'deployer@vilbas.ee'])

@setup
    $repository = 'git@gitlab.vilbas.ee:tak16/tarkvaraprojekt.git';
    $releases_dir = '/var/www/tarkvaraprojekt/master/releases';
    $app_dir = '/var/www/tarkvaraprojekt/master';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    update_symlinks
    node_install
    node_compile
    migrate
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo 'Linking node_modules'
    ln -nfs {{ $app_dir }}/node_modules {{ $new_release_dir }}/node_modules

    echo 'Linking vendor'
    ln -nfs {{ $app_dir }}/vendor {{ $new_release_dir }}/vendor

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask

@task('migrate')
    echo 'Running migrations'
    php {{ $app_dir }}/current/artisan migrate
@endtask

@task('node_install')
    echo 'Installing assets'
    cd {{ $new_release_dir }}
    npm install
@endtask

@task('node_compile')
    echo 'Compiling assets'
    cd {{ $new_release_dir }}
    npm run prod
@endtask