<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class UserVote extends Model {
    protected $table = 'user_vote';

    public function user() {
        return $this->belongsTo(User::class);
    }
}