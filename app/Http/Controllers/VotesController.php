<?php


namespace App\Http\Controllers;


use App\UserVote;
use App\Vote;
use http\Exception\InvalidArgumentException;
use http\Exception\UnexpectedValueException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Throwable;

class VotesController extends Controller {

    public function all() {
        $votes = Vote::with('createdByUser')->orderBy('created_at', 'DESC')->get();

        $userVotes = UserVote::select(['vote_id', 'user_id', 'value'])->get();

        return ['votes' => $votes, 'user_votes' => $userVotes];
    }

    public function getLastVotes() {
        $votes = Vote::with('createdByUser')->orderBy('created_at', 'DESC')->limit(3)->get();
        $userVotes = UserVote::select(['vote_id', 'user_id', 'value'])->get();

        return ['votes' => $votes, 'user_votes' => $userVotes];
    }

    public function store(Request $request) {
        $request->validate([
            'title' => 'required|string|min:5|max:255',
            'description' => 'required|string',
            'isActive' => 'required|boolean'
        ]);

        $vote = new Vote([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'created_by' => $request->user()->id
        ]);

        $vote->saveOrFail();

        return response()->json($vote, Response::HTTP_CREATED);
    }

    public function addPositiveVote(Request $request, Vote $vote) {
        $user = $request->user();
        $userID = $user->id;

        UserVote::where('vote_id', $vote->id)->where('user_id', $userID)->where('value', 0)->delete();
        $voteExists = UserVote::where('vote_id', $vote->id)->where('user_id', $userID)->where('value', 1)->exists();

        if (!$voteExists) {
            $vote->users()->save($user, ['value' => true]);

            return response(null, Response::HTTP_OK);
        }

        return response(null, Response::HTTP_BAD_REQUEST);

    }

    public function addNegativeVote(Request $request, Vote $vote) {
        $user = $request->user();
        $userID = $user->id;


        UserVote::where('vote_id', $vote->id)->where('user_id', $userID)->where('value', 1)->delete();
        $voteExists = UserVote::where('vote_id', $vote->id)->where('user_id', $userID)->where('value', 0)->exists();

        if (!$voteExists) {
            $vote->users()->save($user, ['value' => false]);

            return response(null, Response::HTTP_OK);
        }

        return response(null, Response::HTTP_BAD_REQUEST);
    }

    public function destroy(Request $request, Vote $vote) {
        $user = $request->user();

        if (in_array($user->role, ['admin', 'moderator'])) {
            try {
                $vote->delete();
                return response(null, Response::HTTP_OK);
            } catch (\Exception $exception) {
                return response(['message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        return response(['message' => 'Teil ei ole õigust hääletust kustutada.'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param Request $request
     * @param Vote    $vote
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function update(Request $request, Vote $vote) {
        try {

            if(!in_array($request->user()->role, ['admin', 'moderator']))
                throw new AuthorizationException('Teil ei ole õigust hääletusi muuta.');

            $vote->title = $request->input('title');
            $vote->description = $request->input('description');
            $vote->saveOrFail();
        } catch (AuthorizationException $authorizationException) {
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (Throwable $exception) {
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response(null, Response::HTTP_OK);

    }


}