<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Couchbase\UserSettings;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use GuzzleHttp;
use App\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

//    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Microsoft tenant ID
     * @var string
     */
    private $tenant = '66b93e6b-3bac-45eb-bd8c-91ce24bb1810';

    /**
     * Client ID
     * Our project's ID defined in Azure.
     *
     * @var string
     */
    private $clientId = '6ad93b3d-3ed4-49b5-ac24-9ddfc3d093f2';

    /**
     * Where to redirect the user, once they have logged in on Ametikool's login page.
     *
     * @todo Move to .env file
     * @var string
     */
    private $redirectUri;
    private $clientSecret = 'vJ4hSJGK+u5d/Eo34fw0/BWbgkYq3EwSDcDaeVZdGWU=';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->redirectUri = config('app.url') . '/login';
    }

    /**
     * Authenticates user via Microsoft.
     *
     * @param Request $request
     *
     * @return array
     */
    public function authenticate(Request $request)
    {
        $code = $request->input('code');

        // FOR DEMO PURPOSES
        if(in_array($code, ['admin@demo.com', 'moderator@demo.com', 'user@demo.com'])) {
            $user = User::where('email', $code)->first();

            if (is_object($user)) {
                Auth::loginUsingId($user->id, true);
            } else {

                $role = 'user';

                switch ($code) {
                    case 'admin@demo.com';
                        $role = 'admin';
                        break;
                    case 'moderator@demo.com';
                        $role = 'moderator';
                        break;
                    case 'user@demo.com';
                        $role = 'user';
                        break;
                }

                $user = User::create([
                    'name' => 'Demo User',
                    'email' => $code,
                    'job_title' => 'DEMO',
                    'role' => $role
                ]);

                Auth::loginUsingId($user->id, true);
            }

            return ['token' => $user->createToken('AuthToken')->accessToken];
        }

        $token = $this->requestAccessToken($code);

        try {
            $client = new GuzzleHttp\Client(['base_uri' => 'https://login.microsoftonline.com/']);

            $res = $client->get('https://graph.microsoft.com/v1.0/me', [
                'headers' => [
                    'Authorization' => "Bearer {$token}"
                ]
            ]);

            $responseData = json_decode($res->getBody());
        } catch (GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody()->getContents();
        }

        $user = User::where('email', $responseData->mail)->first();

        if (is_object($user)) {
            Auth::loginUsingId($user->id, true);
        } else {

            $user = User::create([
                'name' => $responseData->displayName,
                'email' => $responseData->mail,
                'job_title' => $responseData->jobTitle,
            ]);

            Auth::loginUsingId($user->id, true);
        }

        return ['token' => $user->createToken('AuthToken')->accessToken];
    }

    private function requestAccessToken($code)
    {
        try {
            $client = new GuzzleHttp\Client(['base_uri' => 'https://login.microsoftonline.com/']);
            $res = $client->post("{$this->tenant}/oauth2/token", [
                'form_params' => [
                    'grant_type' => 'authorization_code',
                    'client_id' => $this->clientId,
                    'code' => $code,
                    'redirect_uri' => $this->redirectUri,
                    'client_secret' => $this->clientSecret,
                    'resource' => 'https://graph.microsoft.com'
                ]
            ]);

            $responseData = json_decode($res->getBody());

            return $responseData->access_token;
        } catch (GuzzleHttp\Exception\ClientException $e) {
            echo $e->getResponse()->getBody()->getContents();
        }
    }

}
