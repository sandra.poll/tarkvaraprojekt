<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CandidatedController extends Controller
{
    /**
     * Save candidation if the user has not already candidated
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        $candidate = new Candidate();
        $user = User::has('candidates')->where('id', Auth::id())->first();

        if (!is_null($user)) {
            $candidateExistingError = "Olete juba kandideerinud!";
            return response()->json(['message' => $candidateExistingError], 409);
        } else {
            $user = Auth::user();
            $user->candidates()->save($candidate);
            $candidate->load('user');
            return response()->json(['message' => 'Kandideerimine õnnestus!', 'candidate' => $candidate], 201);
        }
    }

    /**
     * Check if the user is admin or user is deleting it's own candidation
     * Delete candidation
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, int $id)
    {
        $user = $request->user();

        if ($user->role === 'admin' || $user->id === $id) {
            $user = User::has('candidates')->where('id', $id)->first();

            if (!is_null($user)) {
                $user->candidates()->delete();
                return response()->json(['message' => 'Kandideerimise kustutamine õnnestus!'], 201);
            }
        }
    }

    /**
     * Get all candidates
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCandidates()
    {
        $candidates = Candidate::with('user')->get();
        return response()->json($candidates);
    }

    /**
     * Check if the user is admin
     * Accept the candidate by changing candidates role to moderator
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function acceptCandidate(Request $request, User $user)
    {
        $currentUser = $request->user();

        if ($currentUser->role === 'admin') {
            if (!is_null($user)) {
                $userRole = 'moderator';
                $user->role = $userRole;
                $user->save();
                $user->candidates()->delete();

                return response()->json(['message' => 'Õpilane õpilasesindusse lisatud!'], 201);
            }
        }
    }
}
