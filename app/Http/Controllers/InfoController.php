<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Candidate;
use App\Vote;
use App\User;

class InfoController extends Controller
{
    public function getMembers()
    {
        $members = User::where('role', 'moderator')->get();
        return response()->json($members);
    }

    public function getInfoText()
    {
        $contactInfo = DB::select("select value, kkk from options where kkk in 
       ('infoDescription', 'infoProjects')");

        $mappedInfo = [];
        foreach ($contactInfo as $item) {
            $mappedInfo[$item->kkk] = $item->value;
        }
        return response()->json($mappedInfo);
    }

    public function changeInfoText(Request $request)
    {
        $userRole = $request->input('userRole');

        if ($userRole === 'admin') {
            $key = $request->input('key');
            $value = $request->input('value');

            DB::table('options')->where('kkk', $key)->update(['value' => $value]);

            return response()->json(['message' => $userRole], 201);
        }
    }

    public function destroy(Request $request, $id)
    {
        $user = $request->user();

        if ($user->role === 'admin') {
            $user = User::where('id', $id)->update(['role' => 'user']);
            if (!is_null($user)) {
                return response()->json(['message' => 'Kandideerimise kustutamine õnnestus!'], 201);
            }
        }
    }

    public function getInfo()
    {
        $candidateCount = Candidate::with('user')->count();
        $moderatorCount = User::where('role', 'moderator')->count();
        $userCount = User::count();
        $votesCount = Vote::with('user')->count();

        return [
            'moderatorCount' => $moderatorCount,
            'candidateCount' => $candidateCount,
            'userCount' => $userCount,
            'votesCount' => $votesCount
        ];
    }
}
