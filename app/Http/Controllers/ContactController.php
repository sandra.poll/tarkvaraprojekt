<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    /**
     * Get info from options table and map it to $mappedInfo array
     * @return \Illuminate\Http\JsonResponse
     */
    public function getContactData()
    {
        $contactInfo = DB::select("select value, kkk from options where kkk in 
       ('ContactNumber', 'ContactEmail', 'ContactAddress')");

        $mappedInfo = [];
        foreach ($contactInfo as $item) {
            $mappedInfo[$item->kkk] = $item->value;
        }
        return response()->json($mappedInfo);
    }

    /**
     * Check if the user is admin
     * Update table info
     * Send success message to page
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeContactData(Request $request)
    {
        $userRole = $request->input('userRole');

        if ($userRole === 'admin') {
            $key = $request->input('key');
            $value = $request->input('value');

            DB::table('options')->where('kkk', $key)->update(['value' => $value]);

            return response()->json(['message' => 'Muutmine õmnestus!'], 200);
        }
    }
}
