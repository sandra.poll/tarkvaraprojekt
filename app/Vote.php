<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Vote extends Model {

    protected $fillable = ['title', 'description', 'created_by'];
    protected $appends = ['positive_votes', 'negative_votes'];

    public function users() {
        return $this->belongsToMany(User::class)->withPivot('value');
    }

    public function getPositiveVotesAttribute() {
        $votes = UserVote::where('vote_id', $this->id)->where('value', 1)->get();
        return count($votes);
    }

    public function userVotes() {
        return $this->hasMany(UserVote::class);
    }

    public function getNegativeVotesAttribute() {
        $votes = UserVote::where('vote_id', $this->id)->where('value', 0)->get();
        return count($votes);
    }

    public function createdByUser() {
        return $this->belongsTo(User::class, 'created_by');
    }


}