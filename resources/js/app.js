/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import axios from "axios";


require('./bootstrap');

import Vue from "vue";
import Vuetify from "vuetify";
import App from "./components/App.vue";
import router from "./router";
import {store} from "./store"
// import '../stylus/main.styl';

Vue.use(Vuetify);

const app = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});


/**
 * This is used for setting the authorization token globally.
 * So every request made from Vue is authenticated.
 *
 * @type {string}
 */
const token = localStorage.getItem('userToken');
if (token) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
}
