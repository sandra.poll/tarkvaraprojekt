import Home from '../pages/Home.vue'
import Login from '../pages/Login.vue'
import Candidating from "../pages/Candidating"
import Contact from "../pages/Contact.vue";
import Votes from "../pages/Votes";
import Info from "../pages/Info";
// import Gallery from "../pages/gallery";
// import Calendar from "../pages/Calendar";

// your vuex store
import {store} from '../store'

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        store.commit('SET_LAYOUT', 'simple-layout');
        next();
        return
    }
    next('/')
};

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        store.commit('SET_LAYOUT', 'app-layout');
        next();
        return
    }
    next('/login')
};

export default [
    {name: 'home', path: '/', component: Home, beforeEnter: ifAuthenticated},
    {name: 'login', path: '/login', component: Login, beforeEnter: ifNotAuthenticated},
    {name: 'candidating', path: '/candidating', component: Candidating, beforeEnter: ifAuthenticated},
    {name: 'contact', path: '/contact', component: Contact, beforeEnter: ifAuthenticated},
    {name: 'votes', path: '/votes', component: Votes, beforeEnter: ifAuthenticated},
    // {name: 'gallery', path: '/gallery', component: Gallery, beforeEnter: ifAuthenticated},
    // {name: 'calendar', path: '/calendar', component: Calendar, beforeEnter: ifAuthenticated},
    {name: 'info', path: '/info', component: Info, beforeEnter: ifAuthenticated},
]