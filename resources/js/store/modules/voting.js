import axios from "axios";
import qs from "qs";
import router from "../../router";

import {store} from '../index';

const state = {
    items: [],
    loading: false,
    userVotes: [],
    lastVotes: [],
};

const getters = {
    all: state => state.items,
    last: state => state.lastVotes,
    loading: state => state.loading,
    positiveVotes: (state) => (voteId) => {
        return state.userVotes.filter(userVote => userVote.value === 1 && userVote.vote_id === voteId).length;
    },
    negativeVotes: (state) => (voteId) => {
        return state.userVotes.filter(userVote => userVote.value === 0 && userVote.vote_id === voteId).length;
    },
    hasUserVotedOn: (state) => (voteId, userId) => {
        const userVote = state.userVotes.find(userVote => userVote.vote_id === voteId && userVote.user_id === userId);

        return userVote ? userVote.value : undefined;
    },
    currentUserVotesCount: (state) => (userID) => {
        return state.items.filter( item => item.created_by === userID).length;
    }
};

const actions = {
    load({state, commit}) {
        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true);
            axios
                .get('/api/votes')
                .then(r => r.data)
                .then(data => {
                    commit('SET_ITEMS', data.votes);
                    commit('SET_USER_VOTES', data.user_votes);
                    commit('SET_LOADING', false);
                    resolve(data.votes);
                });
        });
    },
    loadLast({state, commit}) {
        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true);
            axios
                .get('/api/last-votes/')
                .then(r => r.data)
                .then(data => {
                    commit('SET_LAST_VOTES', data.votes);
                    commit('SET_USER_VOTES', data.user_votes);
                    commit('SET_LOADING', false);
                    resolve(data.votes);
                });
        });
    },
    save({commit}, data) {
        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true);
            axios.post('/api/votes', {
                title: data.title,
                description: data.description,
                isActive: data.isActive
            })
                .then(response => response.data)
                .then(vote => {
                    commit('ADD_ITEM', vote);
                    resolve(vote);
                    commit('SET_LOADING', false);
                })
                .catch(error => {
                    reject(error.response.data);
                    commit('SET_LOADING', false);
                });
        });
    },
    vote({commit}, data) {
        return new Promise((resolve, reject) => {
            const user = store.getters.user;
            const userID = user.id;

            if(user.role !== 'admin' && user.role !== 'moderator' && getters.currentUserVotesCount(userID) >= 3) {
                reject();
                return;
            }


            commit('SET_LOADING', true);
            axios.get('/api/votes/' + data.vote.id + '/' + data.value).then(() => {
                switch (data.value) {
                case 'yes':
                    data.value = 1;
                    break;
                case 'no':
                    data.value = 0;
                    break;
                }
                commit('VOTE', data);
                commit('SET_LOADING', false);
                resolve();
            })
                .catch(err => {
                    reject(err);
                });
        });

    },
    update({commit}, vote) {
        return new Promise((resolve, reject) => {
            axios.put('/api/votes/' + vote.id, vote)
                .then(() => {
                    resolve();
                    commit('UPDATE_VOTE', vote)
                })
                .catch(() => {
                    reject();
                });
        })
    },
    delete({commit}, id) {
        return new Promise((resolve, reject) => {
            axios.delete('/api/votes/' + id)
                .then(() => {
                    commit('DELETE_VOTE', id);
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });

        });

    }
};

const mutations = {
    SET_ITEMS(state, votes) {
        state.items = votes
    },
    SET_LAST_VOTES(state, votes) {
        state.lastVotes = votes
    },
    SET_USER_VOTES(state, userVotes) {
        state.userVotes = userVotes;
    },
    ADD_ITEM(state, vote) {
        state.items.unshift(vote);
    },
    SET_LOADING(state, loading) {
        state.loading = loading
    },
    VOTE(state, data) {
        let userVote = state.userVotes.find(userVote => userVote.vote_id === data.vote.id && userVote.user_id === data.user.id);



        if (userVote === undefined) {
            state.userVotes.push({
                value: data.value,
                user_id: data.user.id,
                vote_id: data.vote.id
            });
        } else {
            let userVoteIndex = state.userVotes.indexOf(userVote);
            state.userVotes[userVoteIndex].value = data.value;
        }
    },
    UPDATE_VOTE(state, vote) {
          let item = state.items.filter(item => item.id === vote.id);

          let indexOfItem = state.items.indexOf(item);

          state.items[indexOfItem] = vote;
    },
    DELETE_VOTE(state, id) {
        state.items = state.items.filter(vote => vote.id !== id);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}