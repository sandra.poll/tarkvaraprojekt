'use strict';

import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";

import voting from './modules/voting';
import router from '../router';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        layout: 'simple-layout',
        token: localStorage.getItem('userToken') || '',
        status: '',
        user: {}
    },
    mutations: {
        SET_LAYOUT(state, payload) {
            state.layout = payload
        },
        AUTH_REQUEST(state) {
            state.status = 'loading'
        },
        AUTH_LOGOUT(state) {
            state.token = undefined;
            state.user = {};
        },
        AUTH_SUCCESS(state, token) {
            state.status = 'success';
            state.token = token
        },
        AUTH_ERROR(state) {
            state.status = 'error'
        },
        SET_USER(state, user) {
            state.user = user;
        }
    },
    getters: {
        layout(state) {
            return state.layout
        },
        isAuthenticated: state => !!state.token,
        authStatus: state => state.status,
        user: state => state.user,
        isAdmin: state => state.user.role === 'admin',
        isModerator: state => state.user.role === 'moderator',
        isStaff: state => state.user.role === 'moderator' || state.user.role === 'admin',
    },
    actions: {
        AUTH_REQUEST({commit, dispatch}, code) {
            return new Promise((resolve, reject) => { // The Promise used for router redirect in login
                commit('AUTH_REQUEST');
                axios({url: '/api/auth', data: code, method: 'POST'})
                    .then(resp => {
                        const token = resp.data.token;
                        localStorage.setItem('userToken', token); // store the token in localstorage
                        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;

                        commit('AUTH_SUCCESS', resp);

                        // dispatch('USER_REQUEST');
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('AUTH_ERROR', err);
                        localStorage.removeItem('userToken'); // if the request fails, remove any possible user token if possible
                        reject(err)
                    })
            })
        },
        AUTH_LOGOUT({commit, dispatch}) {
            return new Promise((resolve, reject) => {
                commit('AUTH_LOGOUT');
                localStorage.removeItem('userToken');
                localStorage.removeItem('currentUser');
                delete axios.defaults.headers.common['Authorization'];
                resolve()
            })
        }
    },
    modules: {
        voting
    }
});

